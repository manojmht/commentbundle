<?php

/*
 * This file is part of the FOSCommentBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace FOS\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class CommentType extends AbstractType
{
    private $commentClass;

    public function __construct(string $commentClass)
    {
        $this->commentClass = $commentClass;
    }

    /**
     * Configures a Comment form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('body',CKEditorType::class, array(
            'config' => array(
                'toolbar' => [
                    [
                        'name' => 'row1',
                        'items' => [
                            'Bold', 'Italic', 'Underline', 'Strike', 
                            'Subscript', 'Superscript','-',
                            'TextColor', '-',
                            'NumberedList', 'BulletedList', '-',
                            'Outdent', 'Indent', '-',
                            'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'list', 'indent', 'blocks', 'align', 'bidi', '-',
                            'Link', 'Unlink',
                        ],
                    ],
                ],
                'extraPlugins' => ['mentions', 'ajax', 'autocomplete', 'textmatch', 'textwatcher', 'xml'], 
                'mentions' => [
                    [ 
                            'feed' => '/chatter/mentions?name={encodedQuery}',
							'itemTemplate' =>'<li data-id="{id}">
                            <strong class="last_name">{name}</strong></br>
                            <span class="email">{email}</span>
                            </li>',
                            'outputTemplate' => '<div class="mentions"><a href={href}" class="mentions" target="_new" id={email}>{name}</a>&nbsp;</div>',
                            'minChars' => 0,
							'marker' => '@'
                    ],
                ],   
                'removePlugins' => 'elementspath',
                'uiColor' => '#ffffff',
                'input_sync' => true
            ),
            'plugins' => array(
                'mentions' => array(
                    'path'     => 'build/ckeditor/plugins/mentions/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
                'ajax' => array(
                    'path'     => 'build/ckeditor/plugins/ajax/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
                'autocomplete' => array(
                    'path'     => 'build/ckeditor/plugins/autocomplete/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
                'textmatch' => array(
                    'path'     => 'build/ckeditor/plugins/textmatch/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
                'textwatcher' => array(
                    'path'     => 'build/ckeditor/plugins/textwatcher/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
                'xml' => array(
                    'path'     => 'build/ckeditor/plugins/xml/', // with trailing slash
                    'filename' => 'plugin.js',
                ),
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->commentClass,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fos_comment_comment';
    }
}
